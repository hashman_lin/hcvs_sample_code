# 海青工商 - 手把手教學程式碼集散地

## 上課簡報

* https://docs.google.com/presentation/d/1ct9SExwWIwMwtQ0PHvT4LRiOpFgAH2jonyAV6ReON8Y/edit?usp=sharing

## 程式碼範例

* p.39 計算機
    * [計算機練習一](https://bitbucket.org/hashman_lin/hcvs_sample_code/src/master/calculator.php)
    * [計算機練習二](https://bitbucket.org/hashman_lin/hcvs_sample_code/src/master/count_by_account_lesson_1.php)
    * [計算機練習三](https://bitbucket.org/hashman_lin/hcvs_sample_code/src/master/count_by_account_lesson_2.php)
* [PDO 使用範例](https://bitbucket.org/hashman_lin/hcvs_sample_code/src/master/pdo.php)
* [GET 範例](https://bitbucket.org/hashman_lin/hcvs_sample_code/src/master/test_get.php)
* [POST 範例](https://bitbucket.org/hashman_lin/hcvs_sample_code/src/master/test_post.php)
* [登入範例](https://bitbucket.org/hashman_lin/hcvs_sample_code/src/master/login.php)